import FormMessage from './modules/form-message'
import UploadField from './modules/upload-field'

// eslint-disable-next-line import/prefer-default-export
export const DEFAULT_SETTINGS = {
    formMessage: new FormMessage(),
    UploadField,
}
