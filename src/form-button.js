// eslint-disable-next-line import/prefer-default-export
export class FormButton {
    constructor(form) {
        this.button = /** @type {HTMLButtonElement} */ (form.querySelector(`[type='submit']`))
    }

    enable() {
        this.button.classList.remove('c-button--loading')
        this.button.removeAttribute('disabled')
    }

    disable() {
        this.button.classList.add('c-button--loading')
        this.button.setAttribute('disabled', 'true')
    }
}
