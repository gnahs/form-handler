import { merge } from 'lodash'
import { DEFAULT_SETTINGS } from './default-settings'
import { FormButton } from './form-button'
import { InputMessage } from './modules/input-message'
import UploadField from './modules/upload-field'
import { hasErrors } from './utils/has-errors'
import { hasMessage } from './utils/has-message'

const FILE_INPUT_SELECTOR = '[data-upload]'
class FormHandler {
    constructor(form, settings = {}) {
        this.settings = merge(DEFAULT_SETTINGS, settings)

        if (!document.getElementById(form)) {
            throw Error(`There is no form with the id ${form}`)
        }
        this.$form = /** @type {HTMLFormElement} */ (document.getElementById(form))
        this.sendButton = new FormButton(this.$form)

        this.fetchOptions = {}
        this.url = ''
        this.formData = null
        this.$fileInputs = [...this.$form.querySelectorAll(FILE_INPUT_SELECTOR)]

        this.inputMessages = []

        this.init()
    }

    init() {
        this.$form.addEventListener('submit', this.onSubmit.bind(this))
        this.disableMessageOnFocusOrChange()
        this.$fileInputs.forEach(($fileInput) => {
            new UploadField($fileInput)
        })
    }

    onSubmit(ev) {
        ev.preventDefault()
        this.sendButton.disable()
        this.removeExistingMessages()
        this.getFormData(ev.target)
        this.setRequestConfig(ev.target)
        this.makeRequest()
    }

    removeExistingMessages() {
        // Success/Only error message
        this.settings.formMessage?.remove()

        // Error messages
        this.inputMessages.forEach((inputMessage) => {
            inputMessage.remove()
        })
        this.inputMessages = []
    }

    getFormData(form) {
        this.formData = new FormData(form)
        if (!this.formData.has('_token')) {
            const csrfToken = document.querySelector(`[name='csrf-token']`)
            this.formData.append('_token', csrfToken ? csrfToken.content : false)
        }
    }

    setRequestConfig(form) {
        this.url = form.action
        const method = form.method !== null ? form.method : 'POST'
        this.fetchOptions = {
            method,
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                ...this.settings.headers
            },
            body: this.formData,
        }
    }

    makeRequest() {
        fetch(this.url, this.fetchOptions)
            .then(response => response.json())
            .then((data) => {
                if (hasErrors(data) || !data.result) {
                    // Error function
                    this.handleError(data)
                    return
                }
                // Success function
                this.handleSuccess(data)
            })
            .catch(() => {
                this.resetRecaptcha()

                this.dispatchEvent('Error', { errors: { message: 'Something went wrong' } })

                if(this.settings.formMessage) {
                    this.settings.formMessage
                        .new('error', 'Something went wrong', this.$form)
                        .print()
                    this.scrollToFirstFormError()
                }
            })
            .finally(() => {
                this.sendButton.enable()
            })
    }

    handleSuccess(data) {
        this.dispatchEvent('Success', data)
        if (!this.settings.formMessage) return
        
        this.settings.formMessage
            .new('success', data.message, this.$form)
            .print()
    }

    handleError(errors) {
        this.resetRecaptcha()

        // Generic message when no specific nor general message
        if (!hasErrors(errors) && !hasMessage(errors) && this.settings.formMessage) {
            this.settings.formMessage
                .new('error', 'Something went wrong', this.$form)
                .print()
            this.scrollToFirstFormError()
            return
        }

        // General message when no specific errors
        if (!hasErrors(errors) && hasMessage(errors) && this.settings.formMessage) {
            this.settings.formMessage
                .new('error', errors.message, this.$form)
                .print()
            this.scrollToFirstFormError()
            return
        }
        
        // Messages for inputs with errors
        if (hasErrors(errors)) {
            this.createInputMessages(errors.errors)
            this.scrollToFirstInputError()
            return
        }
    }

    dispatchEvent(type, message = null) {
        const evt = new CustomEvent(`form${type}`, { detail: message })
        this.$form.dispatchEvent(evt)
    }

    createInputMessages(errors) {
        const { message, ...inputErrors } = errors

        this.inputMessages = Object.entries(inputErrors)
            .map(([name, errors]) => new InputMessage(this.$form, name, errors))

        this.inputMessages.forEach(inputMessage => {
            inputMessage.print()
        })
    }

    disableMessageOnFocusOrChange() {
        // Watch input focus
        this.$form.addEventListener('focus', (ev) => {
            this.removeInputErrorMessage(ev.target)
        }, true)

        // Watch checkboxes and select change
        this.$form.addEventListener('change', (ev) => {
            this.removeInputErrorMessage(ev.target)
        }, true)
    }

    // eslint-disable-next-line class-methods-use-this
    removeInputErrorMessage($field) {
        if (!$field.hasAttribute('aria-invalid') && $field.getAttribute('aria-invalid') === 'false') return

        const fieldName = $field.getAttribute('name')
        const inputMessageToRemove = this.inputMessages
            .find(inputMessage => {
                const $input = inputMessage.getInput()
                const name = $input.getAttribute('name')
                return name === fieldName
            })
        inputMessageToRemove?.remove()

        this.inputMessages = this.inputMessages.filter(inputMessage => inputMessage !== inputMessageToRemove)
    }

    scrollToFirstInputError() {
        if (this.inputMessages.length === 0) return
        const $input = this.inputMessages[0].getInput()

        if(!$input) return

        $input.scrollIntoView({
            behavior: 'smooth',
            block: 'center',
            inline: 'center',
        })
    }

    scrollToFirstFormError() {
        if (!this.settings.formMessage) return 

        this.settings.formMessage.getMessage()?.scrollIntoView({
            behavior: 'smooth',
            block: 'center',
            inline: 'center',
        })
    }

    resetRecaptcha() {
        try {
            if (typeof grecaptcha !== 'undefined') {
                grecaptcha.reset()
            }
        } catch {}
    }
}

export default FormHandler
