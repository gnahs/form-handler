import FormHandler from './form-handler'
import FormMessage from './modules/form-message'
import UploadField from './modules/upload-field'

window.FormHandler = FormHandler
export default FormHandler
export {
    FormMessage,
    UploadField,
}
