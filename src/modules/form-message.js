/* eslint-disable class-methods-use-this */
export default class FormMessage {
    constructor(container = null) {
        this.container = container
    }

    /**
     * @param {'success'|'error'} type
     * @param {String} description
     */
    new(type, description, form) {
        this.type = type
        this.description = description
        this.container = this.container || form
        this.$message = this.create(type, description)

        return this
    }

    /**
     * @param {'success'|'error'} type
     * @param {String} description
     */
    create(type, description) {
        const $message = document.createElement('div')
        $message.classList.add(`${type}`)
        $message.classList.add(`c-form-message`)

        const $description = document.createElement('div')
        $description.classList.add('c-form-message__description')
        $description.innerHTML += `<p>${description}</p>`
        $message.appendChild($description)

        return $message
    }

    print() {
        const $container = typeof this.container === 'string'
            ? document.querySelector(this.container)
            : this.container

        if (!$container) {
            throw Error(`Element with this selector (${this.container}) does not exist`)
        }

        $container.appendChild(this.$message)
    }

    remove() {
        this.$message?.remove()
    }

    getMessage() {
        return this.$message
    }
}
