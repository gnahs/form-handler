// eslint-disable-next-line import/prefer-default-export
export class InputMessage {
    constructor($form, fieldName, errors) {
        this.$input = $form.querySelector(`[name='${fieldName}']`)
        this.fieldName = fieldName
        this.errors = errors

        this.$errors = this.create()
        this.setInputHowInvalid()
    }

    create() {
        const $message = document.createElement('div')
        $message.classList.add('error-message')
        $message.classList.add(`error-${this.fieldName}`)

        const errors = this.errors
            .map(message => `<small>${message}</small>`)
            .join('')

        $message.innerHTML = errors

        return $message
    }

    setInputHowInvalid() {
        if (this.isInputFile()) {
            this.$input.parentNode.setAttribute('data-invalid', 'true')
            return
        }
        this.$input.setAttribute('aria-invalid', 'true')
    }

    setInputHowValid() {
        if (this.isInputFile()) {
            this.$input.parentNode.removeAttribute('data-invalid')
            return
        }
        this.$input.removeAttribute('aria-invalid')
    }

    isInputFile() {
        return this.$input.getAttribute('type') === 'file'
    }

    print() {
        this.$input.parentNode.appendChild(this.$errors)
    }

    remove() {
        this.setInputHowValid()
        this.$errors.remove()
    }

    getInput() {
        return this.$input
    }
}
