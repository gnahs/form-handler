export default class UploadField {
    /**
     * @param {String|Element} container 
     * @returns 
     */
    constructor(container = '[data-upload]') {
        this.$container = typeof container === 'string' 
            ? document.querySelector(container)
            : container

        const id = this.$container.getAttribute('data-upload')
        this.$nameField = document.querySelector(`[data-upload-name="${id}"]`)
        this.$field = document.querySelector(`[data-upload-field="${id}"]`)
        this.language = window?.rho?.language || window.navigator.language
        
        this.$field.addEventListener('input', this.onInputFileChange.bind(this))
    }

    onInputFileChange(ev) {
        const { target } = ev

        const fileNames = [...target.files].map(file => file.name)

        const formattedListOfNames = new Intl
            .ListFormat(this.language, { style: 'long', type: 'conjunction' })
            .format(fileNames)

        this.$nameField.textContent = formattedListOfNames
    }
}
