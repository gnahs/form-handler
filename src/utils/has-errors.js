import { hasProperty } from './has-property'

// eslint-disable-next-line import/prefer-default-export
export function hasErrors(request) {
    return hasProperty(request, 'errors')
}
