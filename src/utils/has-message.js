import { hasProperty } from './has-property'

// eslint-disable-next-line import/prefer-default-export
export function hasMessage(request) {
    return hasProperty(request, 'message')
}
