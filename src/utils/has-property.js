// eslint-disable-next-line import/prefer-default-export
export function hasProperty(obj, property) {
    return Object.prototype.hasOwnProperty.call(obj, property)
}
