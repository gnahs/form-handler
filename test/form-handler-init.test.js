/* eslint-disable */
jest.setTimeout(2000)

const urlTest = 'http://form-handler.test'
// const urlVendor = 'http://form-handler.test/public/js/vendor.js'
const urlComponent = 'http://form-handler.test/public/js/form-handler.js'

const devices = require('puppeteer/DeviceDescriptors')
const iPhonex = devices['iPhone X']

const mainSelector = '#contact'

const customBeforeAll = () => {
    beforeAll (async () => {
        await page.goto(urlTest)
        // await page.addScriptTag({ url: urlVendor })
        await page.addScriptTag({ url: urlComponent })
        await page.evaluate(() => {
            new FormHandler('contact')
        })
    })
}


describe ('Init Form handler', () => {

    customBeforeAll()

    it('should create instance', async () => {
        await page.waitForSelector(mainSelector)
        const mainElement = await page.$(mainSelector)
        await expect(page.evaluate(element => element.getAttribute("id"), mainElement)).resolves.toMatch('contact')
    })
})
